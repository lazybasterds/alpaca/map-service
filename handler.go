package main

import (
	"context"
	"log"

	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/alpaca/map-service/proto/map"
)

type service struct {
}

func (s *service) GetRepo() Repository {
	return &MapRepository{}
}

func (s *service) GetPath(ctx context.Context, req *pb.Path, res *pb.NodeResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	start := req.Start
	goal := req.Goal

	nodes, err := repo.GetPath(ctx, start, goal)
	if err != nil {
		log.Printf("Error: Failed to get path from %v to %v : %+v", start, goal, err)
		return err
	}
	log.Printf("Method: GetPath, RequestID: %s, Return: %+v", requestID, nodes)
	res.Nodes = nodes
	return nil
}
