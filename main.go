package main

import (
	"context"
	"log"
	"os"

	micro "github.com/micro/go-micro"
	_ "github.com/micro/go-plugins/registry/kubernetes"
	k8s "github.com/micro/kubernetes/go/micro"
	pb_map "gitlab.com/lazybasterds/alpaca/map-service/proto/map"
	pb_node "gitlab.com/lazybasterds/node-service/proto/node"
)

const (
	defaultHost = "localhost:27017"
)

var (
	srv               micro.Service
	nodeServiceClient pb_node.NodeServiceClient
	buildingMap       []*Node
)

func main() {
	host := os.Getenv("DB_HOST")

	if host == "" {
		host = defaultHost
	}

	ctx := context.Background()
	db, err := CreateDBConnection(ctx, host)

	if err != nil {
		// TODO: Wrap this in a good logging
		log.Panicf("Could not connect to the database with host %s - %v", host, err)
	}
	defer db.Close(ctx)

	srv = k8s.NewService(
		micro.Name("map-service"),
		micro.Version("latest"),
	)

	// Will parse the command line args
	srv.Init()

	initServiceClients(srv)
	buildMap()

	// Register handler
	pb_map.RegisterMapServiceHandler(srv.Server(), &service{})

	// Run the server
	if err := srv.Run(); err != nil {
		log.Printf("ERROR: Failed to run server for map-service - %v", err)
	}
}

func initServiceClients(srv micro.Service) {
	nodeServiceClient = pb_node.NewNodeServiceClient("node-service", srv.Client())
}
