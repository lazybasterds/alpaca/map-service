package main

import (
	"context"
	"log"
	"sync"
	"time"

	"github.com/mongodb/mongo-go-driver/bson/primitive"

	"github.com/jaslife1/uniuri"
	"github.com/micro/go-micro/metadata"
	pb_node "gitlab.com/lazybasterds/node-service/proto/node"
)

func buildMap() {
	reqID := uniuri.New()
	// Call to the grpc
	log.Println("Building map, RequestID: ", reqID)
	ctx := metadata.NewContext(context.Background(), map[string]string{
		"requestID": reqID,
	})
	duration := time.Now().Add(20 * time.Second)
	ctx, cancel := context.WithDeadline(ctx, duration)
	defer cancel()
	resp, err := nodeServiceClient.GetAllNodes(ctx, &pb_node.Node{})
	if err != nil {
		log.Printf("Error: Failed to get all nodes : %+v", err)
		return
	}

	//Convert pb_node type to node type
	buildingMap = convertNodes(resp.Nodes)

	//Connect each node to its neighbor
	var wg sync.WaitGroup
	wg.Add(len(buildingMap))
	for i := range buildingMap {
		go connectNeighbors(&wg, &buildingMap, i)
	}
	wg.Wait()
}

func connectNeighbors(wg *sync.WaitGroup, nodeList *[]*Node, index int) {
	defer wg.Done()
	node := (*nodeList)[index]
	var modelNeighbors []string

	for _, n := range *nodeList {
		if n.ID == node.ID {
			modelNeighbors = n.Neighbors
			break
		}
	}

	for _, n := range modelNeighbors {
		for _, p := range *nodeList {
			tmpObjID, err := primitive.ObjectIDFromHex(n)
			if err != nil {
				log.Println("Failed to get the object id from ", n, ": ", err)
				return
			}
			if p.ID == tmpObjID {
				node.InternalNeighbors = append(node.InternalNeighbors, p)
			}
		}
	}
}

func convertNodes(source []*pb_node.Node) []*Node {
	result := make([]*Node, 0, len(source))
	for _, node := range source {
		n := Node{
			Name: node.Name,
			Type: node.Type,
			Coordinate: Coordinate{
				X: node.Coordinate.X,
				Y: node.Coordinate.Y,
				Z: node.Coordinate.Z,
			},
			FloorID: node.FloorID,
			Details: NodeDetails{
				Description: node.Details.Description,
				Location:    node.Details.Location,
				Category:    node.Details.Category,
				Tags:        node.Details.Tags,
				OpeningHours: OpeningHours{
					Sunday:    node.Details.OpeningHours.Sunday,
					Monday:    node.Details.OpeningHours.Monday,
					Tuesday:   node.Details.OpeningHours.Tuesday,
					Wednesday: node.Details.OpeningHours.Wednesday,
					Thursday:  node.Details.OpeningHours.Thursday,
					Friday:    node.Details.OpeningHours.Friday,
					Saturday:  node.Details.OpeningHours.Saturday,
				},
			},
			Neighbors: node.Neighbors,
		}

		n.ID, _ = primitive.ObjectIDFromHex(node.Id)
		n.Details.LogoID, _ = primitive.ObjectIDFromHex(node.Details.Logoid)
		n.Details.ImageID, _ = primitive.ObjectIDFromHex(node.Details.Imageid)

		result = append(result, &n)
	}

	return result
}

func getNode(graph *[]*Node, x, y, z float64) (node *Node) {
	for _, n := range *graph {
		if n.Coordinate.X == x && n.Coordinate.Y == y && n.Coordinate.Z == z {
			node = n
			break
		}
	}
	return
}

func getNodeByID(graph *[]*Node, id string) (node *Node) {
	objectID, _ := primitive.ObjectIDFromHex(id)
	for _, n := range *graph {
		if n.ID == objectID {
			node = n
			break
		}
	}
	return
}
