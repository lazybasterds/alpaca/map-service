package main

import (
	"github.com/mongodb/mongo-go-driver/bson/primitive"
)

// Node is the most basic component in a Map.
type Node struct {
	ID                primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Name              string             `bson:"name" json:"name"`             // Store name, Airport Gate name
	Type              string             `bson:"type" json:"type"`             // Can be store, stairs, elevator, toilet, escalator, airport gate,kiosk, etc.
	Coordinate        Coordinate         `bson:"coordinate" json:"coordinate"` // X,Y,Z coordinate of the Node in that specific floor
	FloorID           string             `bson:"floorid" json:"floorid"`
	Details           NodeDetails        `bson:"details" json:"details"`
	Neighbors         []string           `bson:"neighbors"`
	InternalNeighbors []*Node
}

// NodeDetails contains the additional details of the Node type
type NodeDetails struct {
	ID           primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Description  string             `bson:"description" json:"description"`
	LogoID       primitive.ObjectID `bson:"logo" json:"logoid"`
	ImageID      primitive.ObjectID `bson:"image" json:"imageid"`
	OpeningHours OpeningHours       `bson:"openinghours" json:"openinghours"`
	Location     string             `bson:"location" json:"location"` // Building specific name
	Category     string             `bson:"category" json:"category"`
	Tags         []string           `bson:"tags" json:"tags"` // Additional subtype like for a store it can be a retail store or book store
	// ImageValue   Image
	// LogoValue    Image
}

// OpeningHours contain the opening hours of a particular Node
type OpeningHours struct {
	Sunday    string `bson:"sunday" json:"sunday"`
	Monday    string `bson:"monday" json:"monday"`
	Tuesday   string `bson:"tuesday" json:"tuesday"`
	Wednesday string `bson:"wednesday" json:"wednesday"`
	Thursday  string `bson:"thursday" json:"thursday"`
	Friday    string `bson:"friday" json:"friday"`
	Saturday  string `bson:"saturday" json:"saturday"`
}

// Coordinate contain the (X,Y,Z) component of a Node.
// This is used for identifying the position of a Node in the Map.
type Coordinate struct {
	X float64 `bson:"x" json:"x"`
	Y float64 `bson:"y" json:"y"`
	Z float64 `bson:"z" json:"z"`
}
